from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
"""
    1.视图函数的第一个参数接受请求：HttpRequest的类对象
    2.必须返回一个相应
"""


def index(request):
    return HttpResponse("ok")
