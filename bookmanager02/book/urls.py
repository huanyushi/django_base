from django.urls import path
from book.views import index, goods, search, register, method, cookie, set_session, get_session, redirect_baidu, \
    CenterView

urlpatterns = [
    path('index/', index),
    path('<cat_id>/<goods_id>', goods),
    path('search/', search),
    path('register/', register),
    path('method/', method),
    path('cookie/', cookie),
    path('setsession/', set_session),
    path('getsession/', get_session),
    path('redirectBaidu/', redirect_baidu),
    path('test/', CenterView.as_view())
]
